import QtQuick 2.0
import Sailfish.Silica 1.0

Label {
    property var icon: osem-not-available
    text: osemicons.get(icon)
    horizontalAlignment: Text.AlignHCenter
    font.family: "opensensemap"
}

