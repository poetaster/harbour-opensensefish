import QtQuick 2.0
import Sailfish.Silica 1.0

Label {
    anchors {
        left: parent.left
        right: parent.right
        leftMargin: Theme.paddingSmall
        rightMargin: Theme.paddingSmall
    }
    verticalAlignment: Text.AlignVCenter
    horizontalAlignment: Text.AlignHCenter
    visible: text ? true : false
    wrapMode: Text.Wrap
    font.pixelSize: Theme.fontSizeSmall
}


