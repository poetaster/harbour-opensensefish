import QtQuick 2.0
import Sailfish.Silica 1.0
import "."

Page {

    id: boxpage

    property int account_index: -1
    property var box
    property int box_index: -1
    property var sensorsmodel: ListModel {}
    property bool loading: false
    property bool account_broken: app.last_broken_account_index==account_index
    property var coverContent: CoverContent {
        text: box.name !== undefined ? box.name : ""
        actionIcon: "image://theme/icon-cover-refresh"
        action: function() {sensorslist.populate(true, true)}
    }

    SilicaListView {
        id: sensorslist
        anchors.fill: parent
        anchors.bottomMargin: Theme.paddingLarge
        spacing: Theme.paddingLarge

        VerticalScrollDecorator {}

        header: PageHeader {
            id: header
            visible: sensorsmodel.count > 0
            title: qsTranslate("box-page", "Sensors")
            description: box.name || "?"
            }

        model: sensorsmodel

        PullDownMenu {
            MenuItem {
                text: qsTranslate("box-page","Refresh")
                onClicked: { sensorslist.populate(true, true) }
            }
            ActionsMenuLabel {}
        }

        ViewPlaceholder {
            id: viewPlaceholder
            text: account_broken ?
                qsTranslate("box-page", "Account is incomplete!")
                : qsTranslate("box-page","This senseBox has no sensors")
            hintText: account_broken ?
                qsTranslate("box-page",
                    "Check the account settings!") :
                qsTranslate("box-page",
                    "This should actually never happen...\n" +
                    "Adding sensors is currently not implemented.")
            enabled: sensorsmodel.count <= 0
                && !boxpage.loading && !python.busy
        }

        BusyIndicator {
            id: busyindicator
            visible: boxpage.loading | python.busy
            anchors.centerIn: parent
            size: BusyIndicatorSize.Large
            running: boxpage.loading | python.busy
        }

        Label {
            id: loadinglabel
            anchors {
                horizontalCenter: parent.horizontalCenter
                bottom: busyindicator.top
                bottomMargin: Theme.paddingLarge
            }
            font.pixelSize: Theme.fontSizeLarge
            color: Theme.highlightColor
            visible: busyindicator.running
            text: app.latest_action
        }

        delegate: ListItem {
            id: sensoritem
            contentHeight: sensorrect.height
            visible: !boxpage.loading

            onClicked: {
                var properties = {
                    "account_index": boxpage.account_index,
                    "sensor_index": model.index,
                    "sensor": box.sensors.sensors[model.index],
                    "box": box,
                    }
                console.debug("Opening sensor with properties "
                    + JSON.stringify(properties))
                app.pageStack.push("SensorPage.qml", properties)
            }

            Rectangle {
                id: sensorrect
                radius: Theme.paddingMedium
                color: Theme.rgba(Theme.highlightColor,
                    Theme.highlightBackgroundOpacity)
                anchors.left: parent.left
                anchors.right: parent.right
                anchors.leftMargin: Theme.horizontalPageMargin
                anchors.rightMargin: Theme.horizontalPageMargin
                height: implicitHeight
                    + sensortitlelabel.height
                    + sensoridlabel.height
                    + sensorvaluelabel.height
                    + sensortypelabel.height
                    + sensortimelabel.height

                OpenSenseMapIcon {
                    anchors.top: parent.top
                    anchors.bottom: parent.bottom
                    anchors.left: parent.left
                    anchors.leftMargin: Theme.horizontalPageMargin
                    id: sensoriconlabel
                    icon: model.icon
                    font.pixelSize: Theme.fontSizeHuge
                    verticalAlignment: Text.AlignVCenter
                }

                ListItemLabel {
                    anchors.left: sensoriconlabel.right
                    id: sensortitlelabel
                    font.pixelSize: Theme.fontSizeMedium
                    font.bold: true
                    text: model.title
                    truncationMode: TruncationMode.Fade
                    verticalAlignment: Text.AlignBottom
                    height: implicitHeight + Theme.paddingMedium
                    horizontalAlignment: Text.AlignHCenter
                }

                ListItemLabel {
                    id: sensoridlabel
                    anchors.left: sensoriconlabel.right
                    anchors.top: sensortitlelabel.bottom
                    font.pixelSize: Theme.fontSizeTiny
                    color: Theme.secondaryColor
                    text: model.id || ""
                    visible: model.id || false
                    truncationMode: TruncationMode.Fade
                    height: visible ? implicitHeight : Theme.paddingSmall
                    verticalAlignment: Text.AlignVCenter
                    horizontalAlignment: Text.AlignHCenter
                }

                ListItemLabel {
                    id: sensorvaluelabel
                    anchors.left: sensoriconlabel.right
                    anchors.top: sensoridlabel.bottom
                    font.pixelSize: model.last_value !== undefined ?
                        Theme.fontSizeLarge : Theme.fontSizeMedium
                    font.bold: model.last_value !== undefined ? true : false
                    text: model.last_value !== undefined ?
                        Number(model.last_value).toLocaleString() + " " +
                        model.unit
                        : qsTranslate("box-page", "no measurements yet")
                    truncationMode: TruncationMode.Fade
                    color: model.last_value !== undefined ?
                        Theme.primaryColor : Theme.secondaryColor
                    height: implicitHeight + Theme.paddingLarge
                    verticalAlignment: Text.AlignVCenter
                    horizontalAlignment: Text.AlignHCenter
                }

                ListItemLabel {
                    id: sensortypelabel
                    anchors.left: sensoriconlabel.right
                    anchors.top: sensorvaluelabel.bottom
                    font.pixelSize: Theme.fontSizeMedium
                    color: Theme.secondaryColor
                    text: model.type || ""
                    visible: model.type ? true : false
                    truncationMode: TruncationMode.Fade
                    verticalAlignment: Text.AlignVCenter
                    horizontalAlignment: Text.AlignHCenter
                    height: visible ? implicitHeight + Theme.paddingSmall :
                        Theme.paddingSmall
                }

                ListItemLabel {
                    id: sensortimelabel
                    anchors.right: parent.right
                    anchors.bottom: parent.bottom
                    font.pixelSize: Theme.fontSizeTiny
                    color: Theme.secondaryColor
                    text: model.last_time ?  (
                        qsTranslate("box-page", "last measurement") + " "
                        + model.last_time.toLocaleString(
                            Qt.locale(), Locale.LongFormat)
                        ) : ""
                    visible: model.last_value !== undefined ? true : false
                    truncationMode: TruncationMode.Fade
                    verticalAlignment: Text.AlignVCenter
                    horizontalAlignment: Text.AlignRight
                    height: visible ?  implicitHeight + Theme.paddingLarge :
                        Theme.paddingSmall
                }


            }

        }

        function populate (refresh, refresh_if_no_boxes) {
            boxpage.loading = true
            python.call(
                'python.app.get_box_of_account_by_gui_index_json',
                [boxpage.account_index, boxpage.box_index,
                    refresh || false, refresh_if_no_boxes || false],
                function(box) {
                    console.debug("Python sent box:\n" +
                        JSON.stringify(box))
                    boxpage.box = box
                    sensorsmodel.clear()
                    for (var i = 0; i < box.sensors.sensors.length; i++) {
                        console.debug("Appending sensor to model: "
                            +JSON.stringify(box.sensors.sensors[i]))
                        sensorsmodel.append(box.sensors.sensors[i])
                    }
                boxpage.loading = false
                })
        }

        Component.onCompleted: {
            populate(false, true)
        }

    }

    onAccount_brokenChanged: {
        if (!account_broken) {
            console.debug("Account seems to not be broken anymore, " +
                "retrying to fill the list")
            boxeslist.populate(true, true)
        }
    }
}



