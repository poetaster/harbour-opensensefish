import QtQuick 2.0
import Sailfish.Silica 1.0

Label {
    anchors.left: parent.left
    anchors.leftMargin: Theme.horizontalPageMargin
    anchors.right: parent.right
    anchors.rightMargin: Theme.horizontalPageMargin
    truncationMode: TruncationMode.Fade
}
