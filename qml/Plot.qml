import QtQuick 2.0
import Sailfish.Silica 1.0
import "."

Image {
    // never cache the image because it prevents reloading the image...
    cache: true
    // keep screen aspect ratio
    height: width * Screen.width / Screen.height
    asynchronous: true // don't block UI while loading
    fillMode: Image.PreserveAspectFit

    // somehow, setting this explicitly works better (or at all) than using
    // property bindings to Image.width...
    property real properWidth: Math.round(
        (app.orientation & Orientation.PortraitMask ?
            Screen.width : Screen.height) - 2 * Theme.paddingLarge)
    property real properHeight: Math.round(
        properWidth * Screen.width / Screen.height)

    // property holding the next source
    property url next_source: ""
    property bool loading: status == Image.Loading

    anchors {
        left: parent.left
        right: parent.right
        margins: Theme.paddingLarge
    }

    MouseArea {
        anchors.fill: parent
        enabled: !parent.loading &&
            python.image_provider_ready && python.plot_backend === true
        onClicked: {
            // parent.reload()
        }
    }

    BusyIndicator {
        id: busyindicator
        anchors.centerIn: parent
        visible: parent.loading || python.plot_backend === undefined
        size: BusyIndicatorSize.Medium
        running: visible
    }

    Label {
        id: loadinglabel
        anchors {
            horizontalCenter: busyindicator.horizontalCenter
            bottom: busyindicator.top
            bottomMargin: Theme.paddingSmall
        }
        font.pixelSize: Theme.fontSizeSmall
        visible: busyindicator.visible
        color: Theme.highlightColor
        text: app.latest_action
    }

    Rectangle {
        anchors.fill: parent
        visible: (!python.image_provider_ready || !python.plot_backend
            || parent.status == Image.Null)
                && python.plot_backend !== undefined
        radius: Theme.paddingMedium
        color: Theme.rgba(Theme.highlightColor,
            Theme.highlightBackgroundOpacity)
        Label {
            anchors.centerIn: parent
            textFormat: Text.RichText
            font.pixelSize: Theme.fontSizeMedium
            text:
                "<style>a:link{color: %1;}</style>".arg(Theme.highlightColor)
                + "<center>" +
                (
                python.plot_backend ?
                    qsTranslate("plot", "Plot backend is ready")
                    :
                    qsTranslate("plot", "Install " +
                        "<a href='%1'>Matplotlib</a> to enable plots")
                            .arg("https://openrepos.net/"
                            + "content/nobodyinperson/python3-matplotlib")
                )
                + "</center>"
            onWidthChanged: doLayout()
            horizontalAlignment: Text.HCenter
            wrapMode: Text.Wrap
            onLinkActivated: {
                     Qt.openUrlExternally(link)
                    }
        }
    }

    function setSourceSize() {
        if (sourceSize.width != properWidth) {
            console.debug(
                "Setting sourceSize.width from %1 to current width %2"
                .arg(sourceSize.width).arg(properWidth))
            sourceSize.width = properWidth
        }
        if (sourceSize.height != properHeight) {
            console.debug(
                "Setting sourceSize.height from %1 to current height %2"
                .arg(sourceSize.height).arg(properHeight))
            sourceSize.height = properHeight
        }
    }

    function setsource_now(url) {
        console.log("Setting plot url to " + url)
        setSourceSize()
        source = url
    }

    Timer {
        id: timer
    }

    function delay(delayTime, cb) {
        timer.interval = delayTime;
        timer.repeat = false;
        timer.triggered.connect(cb);
        timer.start();
    }

    function reload(ignoreCache, alsoInactive) {
        if(Qt.application.state == Qt.ApplicationActive) {
            console.debug("Reloading image %1".arg(source))
        }
        else {
            if (alsoInactive) {
                console.info("Reloading image %1 although app is inactive"
                    .arg(source))
            }
            else {
                console.info("Not reloading image %1 because app is inactive"
                    .arg(source))
                return
            }
        }
        var oldcache = cache
        var oldsource = source
        source = ""
        if (ignoreCache) {
            console.debug("Ignoring image cache")
            // We have to force cache = false and cannot change it back to true
            // because setting cache to true reloads an OUTDATED image from the
            // cache, despite the image just having been freshly reloaded...
            cache = false
        }
        setsource_now(oldsource)
    }

    function setsource(url) {
        if (python.image_provider_ready) {
            setsource_now(url)
        }
        else {
            console.log("Image provider is not ready. " +
                "Cannot set source to %1 yet".arg(url))
            next_source = url
            function setsource_later () {
                if (!next_source) return
                console.log("Image provider state changed to "
                    + python.image_provider_ready)
                if (python.image_provider_ready) {
                    console.log("Image provider is now ready, " +
                        "now setting url to " + next_source)
                    setsource_now(next_source)
                    python.image_provider_readyChanged
                        .disconnect(setsource_later)
                }
            }
            python.image_provider_readyChanged.connect(setsource_later)
        }

    }

    Component.onCompleted: {
        // if the Ambience is changed, reload this image
        Theme.highlightDimmerColorChanged.connect(reload)
        // if orientation is changed, reload this image from cache
        app.orientationChanged.connect(reload)
    }
}
