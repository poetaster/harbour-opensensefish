import QtQuick 2.0
import Sailfish.Silica 1.0
import "."

Page {

    id: boxespage

    property int account_index: -1
    property var boxesmodel: ListModel {}
    property bool loading: false
    property bool account_broken: app.last_broken_account_index==account_index
    property string account_username: ""

    property var coverContent: CoverContent {
        text: account_username
        actionIcon: "image://theme/icon-cover-refresh"
        action: function() {boxeslist.populate(true, true)}
    }

    SilicaListView {
        id: boxeslist
        anchors.fill: parent
        anchors.bottomMargin: Theme.paddingLarge
        spacing: Theme.paddingLarge

        VerticalScrollDecorator {}

        header: PageHeader {
            id: header
            visible: boxesmodel.count > 0 || account_username
            title: qsTranslate("boxes-page", "senseBoxes")
            description: account_username
            }

        model: boxesmodel

        PullDownMenu {
            MenuItem {
                text: qsTranslate("boxes-page","Refresh")
                onClicked: { boxeslist.populate(true, true) }
            }
            ActionsMenuLabel {}
        }

        ViewPlaceholder {
            id: viewPlaceholder
            text: account_broken ?
                qsTranslate("boxes-page", "Account is incomplete!")
                : qsTranslate("boxes-page","This account has no senseBoxes")
            hintText: account_broken ?
                qsTranslate("boxes-page",
                    "Check the account settings!") :
                qsTranslate("boxes-page",
                    "Adding senseBoxes is currently not implemented.")
            enabled: boxesmodel.count <= 0
                && !boxespage.loading && !python.busy
        }

        BusyIndicator {
            id: busyindicator
            visible: boxespage.loading | python.busy
            anchors.centerIn: parent
            size: BusyIndicatorSize.Large
            running: boxespage.loading | python.busy
        }

        Label {
            id: loadinglabel
            anchors {
                horizontalCenter: parent.horizontalCenter
                bottom: busyindicator.top
                bottomMargin: Theme.paddingLarge
            }
            font.pixelSize: Theme.fontSizeLarge
            color: Theme.highlightColor
            visible: busyindicator.running
            text: app.latest_action

        }

        delegate: ListItem {
            id: boxitem
            contentHeight: boxrect.height
            property var sensorsmodel: ListModel {}
            visible: !busyindicator.visible

            onClicked: {
                var properties = {"account_index": boxespage.account_index,
                        "box_index": model.index}
                console.debug("Opening senseBox with properties "
                    + JSON.stringify(properties))
                app.pageStack.push("senseBoxPage.qml", properties)
            }

            Rectangle {
                id: boxrect
                radius: Theme.paddingMedium
                color: Theme.rgba(Theme.highlightColor,
                    Theme.highlightBackgroundOpacity)
                anchors.left: parent.left
                anchors.right: parent.right
                anchors.leftMargin: Theme.horizontalPageMargin
                anchors.rightMargin: Theme.horizontalPageMargin
                height: implicitHeight
                    + boxnamelabel.height + boxdescriptionlabel.height
                    + boxpositionlabel.height + boxgrouplabel.height
                    + boxlastseenlabel.height + boxidlabel.height
                    + sensorsgrid.height

                ListItemLabel {
                    id: boxnamelabel
                    anchors.top: parent.top
                    font.pixelSize: Theme.fontSizeMedium
                    font.bold: true
                    text: model.name
                    truncationMode: TruncationMode.Fade
                    verticalAlignment: Text.AlignBottom
                    height: implicitHeight + Theme.paddingMedium
                }

                ListItemLabel {
                    id: boxidlabel
                    anchors.top: boxnamelabel.bottom
                    font.pixelSize: Theme.fontSizeTiny
                    color: Theme.secondaryColor
                    text: model.id || ""
                    visible: model.id || false
                    wrapMode: Text.WordWrap
                    height: visible ? implicitHeight : Theme.paddingSmall
                    verticalAlignment: Text.AlignVCenter
                }

                ListItemLabel {
                    id: boxgrouplabel
                    anchors.top: boxidlabel.bottom
                    font.pixelSize: Theme.fontSizeSmall
                    color: Theme.secondaryColor
                    text: model.grouptag || ""
                    visible: model.grouptag || false
                    truncationMode: TruncationMode.Fade
                    verticalAlignment: Text.AlignVCenter
                    height: visible ?  implicitHeight + Theme.paddingMedium :
                        Theme.paddingSmall
                }

                Image {
                    id: boxpositionicon
                    source: "image://theme/icon-s-task"
                    anchors {
                        top: boxgrouplabel.bottom
                        left: parent.left
                        leftMargin: Theme.horizontalPageMargin
                    }
                }

                ListItemLabel {
                    id: boxpositionlabel
                    anchors {
                        verticalCenter: boxpositionicon.verticalCenter
                        left: boxpositionicon.right
                        leftMargin: 0
                    }
                    font.pixelSize: Theme.fontSizeSmall
                    textFormat: Text.RichText
                    text: model.exposure + " | "
                        + Number(model.current_lat.toFixed(4)).toLocaleString()
                        +"°" + "<b>"
                        + (model.current_lat > 0 ?
                            qsTranslate("coordinates","N") :
                            qsTranslate("coordinates","S"))
                        + "</b>"
                        + " | "
                        + Number(model.current_lon.toFixed(4)).toLocaleString()
                        +"°" + "<b>"
                        + (model.current_lon > 0 ?
                            qsTranslate("coordinates","E") :
                            qsTranslate("coordinates","W"))
                        + "</b>"
                        + (model.current_height ?
                        " | " + Number(model.current_height.toFixed(2))
                            .toLocaleString()
                        + "<b>m</b>":"")
                    color: Theme.secondaryColor
                    verticalAlignment: Text.AlignVCenter
                    truncationMode: TruncationMode.Fade
                }

                ListItemLabel {
                    id: boxdescriptionlabel
                    anchors.top: boxpositionlabel.bottom
                    font.pixelSize: Theme.fontSizeSmall
                    text: model.description || ""
                    visible: model.description || false
                    wrapMode: Text.WordWrap
                    elide: Text.ElideRight
                    height: visible ? implicitHeight + 2 * Theme.paddingLarge :
                        Theme.paddingLarge
                    verticalAlignment: Text.AlignVCenter
                }

                ListItemLabel {
                    id: boxlastseenlabel
                    anchors.top: boxdescriptionlabel.bottom
                    font.pixelSize: Theme.fontSizeTiny
                    color: Theme.secondaryColor
                    text: model.last_upload_time ?  (
                        qsTranslate("boxes-page", "last measurement") + " "
                        + model.last_upload_time.toLocaleString(
                            Qt.locale(), Locale.LongFormat)
                        ) : ""
                    visible: model.last_upload_time || false
                    truncationMode: TruncationMode.Fade
                    verticalAlignment: Text.AlignVCenter
                    horizontalAlignment: Text.AlignRight
                    height: visible ?  implicitHeight + Theme.paddingLarge :
                        Theme.paddingSmall
                }

                Grid {
                    id: sensorsgrid
                    // this is a quite ugly hack
                    columns: width < (Screen.width<Screen.height?
                        Screen.width:Screen.height) ? 2 : 4
                    columnSpacing: 2 * Theme.paddingLarge
                    rowSpacing: Theme.paddingMedium
                    height: implicitHeight + Theme.paddingMedium

                    anchors {
                        top: boxlastseenlabel.bottom
                        left: parent.left
                        right: parent.right
                        leftMargin: Theme.horizontalPageMargin
                        rightMargin: Theme.horizontalPageMargin
                    }

                    Repeater {
                        model: boxesmodel.get(index).sensors.sensors || []

                        Row {
                            spacing: Theme.paddingMedium

                            OpenSenseMapIcon {
                                id: iconlabel
                                icon: modelData.icon
                                font.pixelSize: Theme.fontSizeSmall
                            }

                            Label {
                                id: datalabel
                                anchors.verticalCenter:iconlabel.verticalCenter
                                text: modelData.last_value !== undefined ?
                                Number(modelData.last_value).toLocaleString()
                                    :"?"
                                font.pixelSize: Theme.fontSizeSmall
                            }

                            Label {
                                id: unitlabel
                                anchors.verticalCenter:iconlabel.verticalCenter
                                text: modelData.unit || ""
                                font.pixelSize: Theme.fontSizeSmall
                            }

                        }

                    }
                }


            }

            menu: ContextMenu {

                MenuItem {
                    text: qsTranslate("boxes-page", "open weblink")
                    enabled: model.weblink || false
                    onClicked: {
                        console.info("Opening weblink " + model.weblink)
                        Qt.openUrlExternally(model.weblink)
                    }
                }

            }

        }

        function populate (refresh, refresh_if_no_boxes) {
            boxespage.loading = true
            python.call('python.app.accounts_in_gui_order_json', [],
                function(accounts) {
                    console.debug("Python sent accounts:\n"
                        + JSON.stringify(accounts))
                    var account = accounts[boxespage.account_index]
                    boxespage.account_username =
                        account.username || account.email
                })
            python.call('python.app.get_boxes_of_account_by_gui_index_json',
                [boxespage.account_index, refresh || false,
                    refresh_if_no_boxes || false],
                function(boxes) {
                    console.debug("Python sent senseBoxes:\n" +
                        JSON.stringify(boxes))
                    boxesmodel.clear()
                    for (var i = 0; i < boxes.length; i++) {
                        console.debug("Appending senseBox to model: "
                            +JSON.stringify(boxes[i]))
                        boxesmodel.append(boxes[i])
                    }
                boxespage.loading = false
                })
        }

        Component.onCompleted: {
            populate(false, true)
        }

    }

    onAccount_brokenChanged: {
        if (!account_broken) {
            console.debug("Account seems to not be broken anymore, " +
                "retrying to fill the list")
            boxeslist.populate(true, true)
        }
    }
}


