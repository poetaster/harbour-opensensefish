import QtQuick 2.0
import io.thp.pyotherside 1.4

Python {
    id: python

    property bool busy: true
    property bool imported: true
    property bool app_loaded: false
    property var plot_backend: undefined
    property bool image_provider_ready: false

    function backend_busy ()  { busy = true }
    function backend_ready () { busy = false }

    Component.onCompleted: {
        addImportPath(Qt.resolvedUrl('..'));
        importModule('python', function () {})
        setHandler("imported", function() {
            console.debug("Python module was imported")
            imported = true
        })
        setHandler("app-loaded", function() {
            console.info("Python app was loaded");
            app_loaded = true
            backend_ready()
        })
        setHandler("ready", function() { backend_ready() })
        setHandler("busy", function(task) { backend_busy() })
        setHandler("plot-backend-available", function(available) {
            if (available)
                console.debug("Python says the plot backend is available");
            else
                console.debug("Python says the plot backend is not available");
            plot_backend = available
        })
        setHandler("image-provider-ready", function(ready) {
            if (ready)
                console.debug("Python says the image provider is ready");
            else
                console.debug("Python says the image provider is not ready");
            image_provider_ready = ready
        })
     }

    onError: {
        // when an exception is raised, this error handler will be called
        console.log('python error: ' + traceback);
    }

    onReceived: {
        // asychronous messages from Python arrive here
        // in Python, this can be accomplished via pyotherside.send()
        console.log('got message from python: ' + data);
    }
}

